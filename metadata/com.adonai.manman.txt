AntiFeatures:NonFreeDep
Categories:System
License:GPLv3
Web Site:https://github.com/Adonai/Man-Man/blob/HEAD/README.md
Source Code:https://github.com/Adonai/Man-Man
Issue Tracker:https://github.com/Adonai/Man-Man/issues

Auto Name:Man Man
Summary:View Linux man pages
Description:
Convenient tool for any Linux enthusiast familiar with man pages. It provides
a fast way to search, browse and save man pages via [http://www.mankier.com/ mankier.com].

Features:

* Supports searching for a single command
* Supports explaining command one-liners
* Supports browsing and indexing of man chapters
* Supports caching of man pages that were previously accessed
.

Repo Type:git
Repo:https://github.com/Adonai/Man-Man

Build:1.0.1 Beta,101
    commit=1.0.2
    subdir=app
    gradle=yes

Build:1.0.2 Beta,102
    commit=fa561214d97e9cb67f73b9dc7c6e301f65d99e5f
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.1 Beta
Current Version Code:101

